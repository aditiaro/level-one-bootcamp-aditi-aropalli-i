//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>

float input()
{
	float a;
	printf("\nEnter number: ");
	scanf("%f", &a);
	return a;
}

float add( float a, float b)
{
	float sum = a + b;
	return sum;
}

void output(float d, float o, float g)
{
	printf("\nThe sum of %f and %f is %f", d, o, g);
}

int main()
{
	float c, a, t;
	printf("\nEnter the two numbers you want to add");
	c = input();
	a = input();
	t = add(c,a);
	output(c,a,t);
	return 0;
}
	
	
	